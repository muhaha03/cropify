<?php
/**
 * Cropify Controlelr
 *
 * The controller that encompasess the server-side logic for Cropify's main tasks: upload, crop & delete.
 * 
 * @author 		Alex Seman <mail@alexseman.com>
 * @link 		http://codehub.alexseman.com/cropify
 * @license 	WTFPL (http://sam.zoy.org/wtfpl/COPYING)
 */
	class CropifyController extends CropifyAppController {
/**
 * Cropify's own upload directory where the selected images will be
 * initially uploaded before the cropping operation.
 * It need to have the necessary read and write permissions.
 * 
 * @var string
 * @access private
 */
		private $uploadDir;


/**
 * Called before every action, sets the uploadDir and in case the Auth component 
 * is used, it instructs it to publicly allow the upload, crop & delete actions.
 * 
 * @return void
 * @access public
 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->uploadDir = APP . 'Plugin/Cropify/webroot/uploads/';
			
			if(isset($this->components['Auth'])) {
				$this->Auth->allow('upload', 'crop', 'delete');
			}
		}


/**
 * Uploads an image (received through $_FILES) selected by the user to Cropify's upload directory.
 * It will echo a JSON alone with image data in case the browser has FormData support 
 * or the JSON wrapped around a string containing a script tag with the callback otherwise.
 * Keys for the JSON response:
 *    `result` true if uploaded succeeded, false otherwise
 *    `absolutePath` the absolute system path (on the server) of the uploaded image
 *    `message` the application relative path (of full URL) to the image if the uploaded succeeded or the error message otherwise
 *    `sourceWidth` the original width of the image
 *    `sourceHeight` the original height of the image
 * 
 * @param  string $iframe Name of the callback associated to the iframe that POSTed the data. The callback will be called with the upload's action results.
 * @return mixed 		      
 * @access public
 */
		public function upload($iframe = null) {
			$this->autoRender = false;

			$uploadComponent = $this->Components->load('Cropify.BasicFileUpload');
			$upload = $uploadComponent->upload($_FILES[key($_FILES)], $this->uploadDir);
			$imageSize = array();
			$absolutePath = '';

			if($upload['success']) {
				$this->response->statusCode(200);
				$absolutePath = $this->uploadDir . $upload['filename'];
				$imageSize = getimagesize($absolutePath);
			}
			else {
				$this->response->statusCode(500);
				$imageSize = array(0 => 0, 1 => 0);
			}

			$response = array(
				'result' => $upload['success'],
				'absolutePath' => $absolutePath,
				'message' => $upload['message'],
				'sourceWidth' => $imageSize[0],
				'sourceHeight' => $imageSize[1]
			);

			if(!$iframe) {
				echo json_encode($response);
			}
			else {
				echo '<script type="text/javascript">window.top.window[\'' . $iframe . '\'](' . json_encode($response) . ')</script>';
			}
		}


/**
 * Crops an image to the desired width and height starting from a selected X & Y point.
 * The POST request keys are:
 *    `action` it's value needs to be set to "crop" to make sure the request is valid
 *    `sourceImage` the image to be cropped
 *    `uploadDir` where to place the cropped image
 *    `sourceWidth` the width of the source image
 *    `sourceHeight` the height of the source image
 *    `destWidth` the desired width for the new image
 *    `destHeight` the desired height of the new image
 *    `sourceX` the X of the point from where to start the cropping
 *    `sourceY` the Y of the point from where to start the cropping
 *    `bucket` if present it tries to upload the new image to the S3 bucket specified as value
 * It echoes a JSON containing:
 *    `result` boolean with the success of the operation
 *    `storablePath` easy to munch path for Router::url, generally stored in a data source for future use
 *    `appRelativePath` full web path/URL of the cropped image
 *    `absolutePath` the full system path to the image
 * In case Cropify is coupled with the S3 plugin, all the paths from the response will be the same URL pointing to the image
 * stored on S3.
 *
 * @see    http://codehub.alexseman.com/s3-plugin
 * @return void
 * @access public
 */
		public function crop() {
			$this->autoRender = false;

			if(isset($_POST['action']) && $_POST['action'] == 'crop') {
				$success = true;
			
				$src = $_POST['sourceImage'];
				$sourceX = $_POST['sourceX'];
				$sourceY = $_POST['sourceY'];
				$destWidth = $_POST['destWidth'];
				$destHeight = $_POST['destHeight'];
				$sourceWidth = $_POST['fromWidth'];
				$sourceHeight = $_POST['fromHeight'];
				
				$basename = basename($src);
				$destImage = WWW_ROOT . $_POST['uploadDir'] . '/' . $basename;
				$src = $this->uploadDir . $basename;

				$storablePath = '/' . $_POST['uploadDir'] . '/' . $basename;
				$appRelativePath = Router::url($storablePath);
			
				$pathInfo = getimagesize($src);
				$mime = $pathInfo['mime'];
			
				switch($mime) {
					case 'image/jpeg':	$srcImg = imagecreatefromjpeg($src);
										$dest = imagecreatetruecolor($destWidth, $destHeight);
										
										if(!imagecopyresampled($dest, $srcImg, 0, 0, $sourceX, $sourceY, $destWidth, $destHeight, $sourceWidth, $sourceHeight))	{
											$success = false;
										}
										
										if(!imagejpeg($dest, $destImage, 90)) {
											$success = false;
										}
										
										break;
										
					case 'image/png':	$srcImg = imagecreatefrompng($src);
										$dest = imagecreatetruecolor($destWidth, $destHeight);
										
										if(!imagecopyresampled($dest, $srcImg, 0, 0, $sourceX, $sourceY, $destWidth, $destHeight, $sourceWidth, $sourceHeight))	{
											$success = false;
										}
										
										if(!imagepng($dest, $destImage, 1))	{
											$success = false;
										}
										
										break;
				}
				
				if($success && isset($_POST['bucket'])) {
					$s3Component = $this->Components->load('S3.S3');
					$bucket = base64_decode($_POST['bucket']);
					$url = $s3Component->upload(
						$bucket,
						$destImage,
						basename($_POST['uploadDir']) . '/' . $basename,
						$mime
					);

					if($url === false) {
						echo json_encode(array('result' => $success, 'storablePath' => $storablePath, 'appRelativePath' => $appRelativePath, 'absolutePath' => $destImage));
					}
					else {
						echo json_encode(array('result' => $success, 'storablePath' => $url, 'appRelativePath' => $url, 'absolutePath' => $url));
					}
				}
				else {
					echo json_encode(array(
							'result' => $success,
							'storablePath' => $storablePath,
							'appRelativePath' => $appRelativePath,
							'absolutePath' => $destImage
						)
					);
				}
			}
			else {
				echo json_encode(array(
						'result' => false,
						'postData' => $_POST
					)
				);
			}
		}


/**
 * Called to delete the initial uploaded image after cropping.
 * If the `bucket` key is present then it will delete the image from the S3 bucket.
 * 
 * @return void
 * @access public
 */
		public function delete() {
			$this->autoRender = false;

			if(isset($_POST['action']) && $_POST['action'] == 'delete')	{
				if(isset($_POST['bucket']))	{
					$s3Component = $this->Components->load('S3.S3');
					$bucket = base64_decode($_POST['bucket']);

					$pathInfo = pathinfo($_POST['sourceImage']);
					$dir = basename($pathInfo['dirname']);

					$s3Component->delete($bucket, $dir . '/' . $pathInfo['basename']);
				}
				else {
					unlink($_POST['sourceImage']);
				}
			}
		}
	}
?>

<?php
/**
 * Basic File Upload Component
 *
 * @author 		Alex Seman <mail@alexseman.com>
 * @link 		http://codehub.alexseman.com/cropify
 * @license 	WTFPL (http://sam.zoy.org/wtfpl/COPYING)
 */
	class BasicFileUploadComponent extends Component {

/**
 * Straightforward upload functionality slightly tailored for Cropify
 * by hardcoding the directory part of the URL returned.
 * 
 * @param  array  $file_data typical $_FILES element
 * @param  string $uploadDir the absolute path to the upload directory
 * @return array            summary of the upload operation's outcome. Keys:
 *    `success` boolean true if move_uploaded_file succeeded, false if not
 *    `message` URL of the image if uploaded successfull, error message otherwise
 *    `filename` basename of the newly created file
 * @access public
 */
		public function upload($file_data, $uploadDir) {
			$message = '';
			$success = false;

			$newFilename = uniqid() . str_replace(' ', '', $file_data['name']);
			$path = Router::url('/Cropify/uploads/') . $newFilename;

			switch($file_data['error']) {
				case 0:		if(move_uploaded_file($file_data['tmp_name'], $uploadDir . $newFilename)) {
								$message = $path;
								$success = true;
							}
							else {
								$message = __('File upload failed!');
							}
							break;
						
				case 1:		$message = __('Larger than upload_max_filesize');
							break;
							
				case 2:		$message = __('Larger than MAX_FILE_SIZE');
							break;
							
				case 3:		$message = __('Partial upload');
							break;
							
				case 4:		$message = __('No file');
							break;
				
				case 6:		$message = __('No temporary directory');
							break;
							
				case 7:		$message = __('Can\'t write to disk');
							break;
							
				case 8:		$message = __('File upload stopped by extension');
							break;
			}
			
			return array(
				'success' => $success,
				'message' => $message,
				'filename' => $newFilename
			);
		}
	}
?>

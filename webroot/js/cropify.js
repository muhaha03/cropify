$.widget('cakecms.cropify', {
	options: {
		lang: 'en',
		transl: {
			'en': {
				afterUploadButtonCaption: 'select a different image',
				'uploadFailure': 'Error occurred while uploading file',
				'noConnection': 'Cannot connect. Verify network connection',
				'notFound': 'Requested action not found [404]',
				'serverError': 'Internal Server Error [500]'
			},
			'nl': {
				afterUploadButtonCaption: 'select a different image',
				'uploadFailure': 'Error occurred while uploading file',
				'noConnection': 'Cannot connect. Verify network connection',
				'notFound': 'Requested action not found [404]',
				'serverError': 'Internal Server Error [500]'
			}
		}
	},

	_create: function() {
		document.body.classList.add('cropify-overlayable');

		this.$image = $('<img alt="Loading image&hellip;" >');
		this.$modal = $('#cropify-modal');
		this.$progressBar = this.$modal.find('progress');
		this.$cropButton = this.$modal.find('#cropify-crop');
		this.$cancelButton = this.$modal.find('#cropify-cancel');
		this.emSize = parseInt(window.getComputedStyle(document.body, null).getPropertyValue('font-size'), 10);

		this._bindEvents();
	},

	_init: function() {
		this.$label = this.element.find('label');
		this.$uploadButton = this.element.find('button');
		this.$fileInput = this.element.find('[type="file"]');
		this.$dataField = this.element.find('[type="hidden"]');
		this.data = {};
		this.data.showDestImage = Boolean(parseInt(this.element.data('dest-image')));
		this.data.errorClass = this.element.data('errorClass') || 'cropify-error';
		this.data.afterCropClass = this.element.data('afterCropClass') || 'cropify-after-crop';
		this.data.destWidth = this.$fileInput.data('destWidth');
		this.data.destHeight = this.$fileInput.data('destHeight');
		this.data.cropInitiated = false;
		this.data.cropVars = {
			'sourceX': 0,
			'sourceY': 0,
			'fromWidth': 0,
			'fromHeight': 0
		};

		this._bindBoxEvents();
	},

	_bindEvents: function() {
		this.$cropButton.on(
			'click',
			$.proxy(
				function() {
					this._setWaitCursor();

					$.ajax({
						url: this.$modal.data('cropAction'),
						type: 'POST',
						async: true,
						beforeSend: $.proxy(
							function() {
								var $prevImage = this.element.find('img'),
										original = $prevImage.data('original');

								if($prevImage.length > 0) {
									if(original === undefined || original != $prevImage.attr('src')) {
										this.deleteImage($prevImage.data('absolutePath'));
									}

									$prevImage.remove();
								}
							},
							this
						),
						cache: false,
						context: this,
						dataType: 'json',
						data: $.extend(
							this.data.cropVars,
							{
								'action': 'crop',
								'sourceImage': this.$srcImage.attr('src'),
								'uploadDir': this.$fileInput.data('uploadDir'),
								'destWidth': this.data.destWidth,
								'destHeight': this.data.destHeight
							}
						)
					})
					.done(function(responseData) {
						this._insertCroppedImage(responseData);
					})
					.always(function() {
						this.data.cropVars = {
							'sourceX': 0,
							'sourceY': 0,
							'fromWidth': 0,
							'fromHeight': 0
						};

						this.$modal.removeClass('loaded');
						this._emptyModal();
						this._removeWaitCursor();
					});
				},
				this
			)
		);

		this.$cancelButton.on(
			'click',
			$.proxy(
				function() {
					this.deleteImage(this.$srcImage.data('absolutePath'));
					this.$modal.removeClass('loaded');
					this._emptyModal();
				},
				this
		));
	},

	_bindBoxEvents: function() {
		this.$uploadButton.on(
			'click',
			$.proxy(
				this._clickOn,
				this,
				this.$fileInput
		));

		this.$fileInput.on(
			'change',
			$.proxy(this._filesListener, this)
		);
	},

	_filesListener: function() {
		var imageFile = this.$fileInput.get(0).files[0];

		if(this._isImage(imageFile)) {
			this._placeOverlay();
			this._setWaitCursor();
			this._uploadImage(imageFile);
		}
		else {
			//TODO place translatable error message
			this._reset();
		}
	},

	_uploadImage: function(imageFile) {
		var uploadData = new FormData(),
				oReq = new XMLHttpRequest(),
				updateProgress = function updateProgress(ev) {
					if(ev.lengthComputable) {
						//noinspection JSPotentiallyInvalidUsageOfThis
						this.$progressBar.val((ev.loaded / ev.total) * 100);
					}
				},
				uploadComplete = function uploadComplete(xhr) {
					var errorText = '';

					if(xhr.status === 0) {
						errorText = this.options.transl[this.options.lang].noConnection;
					}
					else if(xhr.status === 404) {
						errorText = this.options.transl[this.options.lang].notFound;
					}
					else if(xhr.status === 500) {
						errorText = this.options.transl[this.options.lang].serverError;
					}
					else if(xhr.status === 200) {
						this.$progressBar.removeAttr('value').prop('indeterminate', true);
						this._prepareCrop.call(this, window.JSON.parse(xhr.response));
					}

					if(errorText !== '') {
						this.$label.text(errorText);
						this.$cancelButton.addClass('cropify-retry');
						this.$modal.removeClass('loading');
						this._reset();
					}
				},
				uploadError = function uploadError() {
					this.$label.text(this.options.transl[this.options.lang].uploadFailure);
					this.$cancelButton.addClass('cropify-retry');
					this.$modal.removeClass('loading');
					this._reset();
				};

		this.$modal.addClass('loading');

		uploadData.append(imageFile.name, imageFile);
		uploadData.append('action', 'upload');

		oReq.addEventListener(
			'progress',
			$.proxy(updateProgress, this),
			false
		);
		oReq.addEventListener(
			'load',
			$.proxy(uploadComplete, this, oReq),
			false
		);
		oReq.addEventListener(
			'error',
			$.proxy(uploadError, this),
			false
		);
//		oReq.responseType = 'json';
		oReq.open(
			'POST',
			this.$modal.data('uploadAction'),
			true
		);
		oReq.send(uploadData);
	},

	_prepareCrop: function(uploadResp) {
		this.$srcImage = this.$image.clone().attr({
			'id': 'source',
			'src': uploadResp.message,
			'data-source-width': uploadResp.sourceWidth,
			'data-source-height': uploadResp.sourceHeight,
			'data-absolute-path': uploadResp.absolutePath
		});

		this.$destImage = this.$srcImage.clone().attr({
			'id': 'dest'
//			'width': this.data.destWidth,
//			'height': this.data.destHeight
		});

		this.$destImage.prependTo(this.$modal).wrap('<div></div>');
		this.$srcImage.prependTo(this.$modal).wrap('<div></div>');

		this.$srcImage.on(
			'load',
			$.proxy(this._initiateCrop, this)
		);

//		if(!this.data.showDestImage) {
			this.$modal.addClass('no-dest');
//		}
//		else {
//			this.$modal.removeClass('no-dest');
//			this.$destImage.parent().css({
//				'width': this.data.destWidth + 'px',
//				'height': this.data.destHeight + 'px'
//			});
//		}

		this.$uploadButton.text(this.options.transl[this.options.lang].afterUploadButtonCaption);
	},

	_initiateCrop: function() {
		var cropRatio = 1,
				cropVars = this.data.cropVars,
				rx = 0,
				ry = 0,
				modalWidth = window.innerWidth - this.emSize * 2,
				modalHeight = window.innerHeight - this.emSize * 2,
				sourceWidth = this.$srcImage.data('sourceWidth'),
				sourceHeight = this.$srcImage.data('sourceHeight'),
				destWidth = this.data.destWidth,
				destHeight = this.data.destHeight,
				cropImage = function cropImage(coords) {
					rx = destWidth / coords.w;
					ry = destHeight / coords.h;

//					cropVars.sourceX = coords.x * cropRatio;
//					cropVars.sourceY = coords.y * cropRatio;
//					cropVars.fromWidth = coords.w * cropRatio;
//					cropVars.fromHeight = coords.h * cropRatio;

					cropVars.sourceX = coords.x;
					cropVars.sourceY = coords.y;
					cropVars.fromWidth = coords.w;
					cropVars.fromHeight = coords.h;
				};

		this.$modal.addClass('loaded');
		this.$modal.removeClass('loading');
		this._reset();

		if(sourceWidth > modalWidth) {

			// Determining new height: original height / original width * new width = new height
			sourceHeight = Math.round(sourceHeight / sourceWidth * modalWidth);
			sourceWidth = modalWidth;

			this.$srcImage.attr({
				'width': sourceWidth,
				'height': sourceHeight
			});
			this.$modal.addClass('full-width');
		}

		if(sourceHeight > modalHeight) {

			// Determining new width: original width / original height * new height = new width
			sourceWidth = Math.round(sourceWidth / sourceHeight * modalHeight);
			sourceHeight = modalHeight;

			this.$srcImage.attr({
				'width': sourceWidth,
				'height': sourceHeight
			});
		}

//		if((sourceWidth + this.data.destWidth + 2 * this.emSize) > modalWidth && (sourceHeight + this.data.destHeight + 2 * this.emSize) > modalHeight) {
//			this.$modal.addClass('no-dest');
//		}

		cropRatio = sourceWidth / this.$srcImage.data('sourceWidth');

//		if(this.data.showDestImage) {
//			cropImage = function(coords) {
//
//				rx = destWidth / coords.w;
//				ry = destHeight / coords.h;
//
//				sourceX = coords.x * cropRatio;
//				sourceY = coords.y * cropRatio;
//				fromWidth = coords.w * cropRatio;
//				fromHeight = coords.h * cropRatio;
//
//				$this.$destImage.css({
//					'width': Math.round(rx * sourceWidth) + 'px',
//					'height': Math.round(ry * sourceHeight) + 'px',
//					'marginLeft': -Math.round(rx * coords.x) + 'px',
//					'marginTop': -Math.round(ry * coords.y) + 'px'
//				});
//			};
//		}
//		else {
//			cropImage = function(coords) {
//				rx = destWidth / coords.w;
//				ry = destHeight / coords.h;
//
//				sourceX = coords.x * cropRatio;
//				sourceY = coords.y * cropRatio;
//				fromWidth = coords.w * cropRatio;
//				fromHeight = coords.h * cropRatio;
//			};
//		}

		this.$srcImage.Jcrop({
			aspectRatio: this.data.destWidth / this.data.destHeight,
			setSelect: [0, 0, this.data.destWidth, this.data.destHeight],
			trueSize: [this.$srcImage.data('sourceWidth'), this.$srcImage.data('sourceHeight')],
			onSelect: cropImage,
			onChange: cropImage
		});
	},

	_insertCroppedImage: function(response) {
		var $cropped = $('<p></p>');

		if(response.result) {
			this.$image.clone().attr({
				'src': response.appRelativePath,
				'alt': response.appRelativePath,
				'data-absolute-path': response.absolutePath
			})
			.appendTo($cropped);

			this.$dataField.val(response.storablePath);
			this.$fileInput.before($cropped);
			this.element.addClass(this.data.afterCropClass);
		}
	},

	_reset: function() {
		this._removeOverlay();
		this._removeWaitCursor();
		this._resetFileInput(this.$fileInput);
		this.$progressBar.prop('indeterminate', false);
		this.$modal.removeClass('full-width');
		this.element.removeClass(this.data.afterCropClass);
	},

	_emptyModal: function() {
		this.$modal.find('div').remove();
		delete this.$srcImage;
		delete this.$destImage;
	},

	// 'change' event isn't triggered on the input type="file" if the same file is immediately re-uploaded
	_resetFileInput: function(fileInput) {
		try {
			fileInput.value = '';
			if(fileInput.value) {
				fileInput.type = 'text';
				fileInput.type = 'file';
			}
		}
		catch(e) { }
	},

	// Programmatically triggers a click event on it's argument
	_clickOn: function(clickable) {
		var event;

		if(typeof clickable.click === 'function') {
			clickable.click();
		}
		else if(document.createEvent) {
			event = document.createEvent('MouseEvents');
			event.initMouseEvent('click', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			clickable.dispatchEvent(event);
		}
		else {
			event = document.createEventObject();
			clickable.fireEvent('onclick', event);
		}
	},

//	_genId: function(prefix) {
//		return prefix + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
//	},

	_isImage: function (file) {
		return file.type.match(/image.*/);
	},

	_removeOverlay: function() {
		document.body.classList.remove('cropify-overlayed');
	},

	_placeOverlay: function() {
		document.body.classList.add('cropify-overlayed');
	},

	_setWaitCursor: function() {
		document.body.classList.add('wait');
	},

	_removeWaitCursor: function() {
		document.body.classList.remove('wait');
	},

	/************* Public *************/

	deleteImage: function(imagePath) {
		$.ajax({
			url: this.$modal.data('deleteAction'),
			type: 'POST',
			async: true,
			data: {
				'action': 'delete',
				'sourceImage': imagePath
			}
		});
	}
});

<?php 
	App::uses('AppHelper', 'View/Helper');

	/**
	 * Cropify Helper
	 * 
	 * The Helper that will place all the necessary markup for Cropify to do it's job 
	 * and tailor it after your settings.
	 * 
	 * @author 		Alex Seman <mail@alexseman.com>
	 * @link 		http://codehub.alexseman.com/cropify
	 * @license 	WTFPL (http://sam.zoy.org/wtfpl/COPYING)
	 */
	class CropifyHelper extends AppHelper
	{
		/**
		 * true to include Cropify's own jQuery library and 
		 * false if there will already be one in the page.
		 * 
		 * @var boolean
		 * @access  private
		 */
		private $includeJQuery = true;

		/**
		 * Name of the WWW_ROOT directory where the files will be uploaded after cropping.
		 * It needs to have the necessary read and write permissions.
		 * Defaults to WWW_ROOT/uploads and it's not necessary if Cropify is coupled with 
		 * the S3 plugin.
		 *
		 * @var string
		 * @access  private
		 */
		private $uploadDir = 'uploads';

		/**
		 * Options array that need to be set for each instance of Cropify->input() call. 
		 * Possible keys are shown in the docblock for Cropify->input().
		 *    
		 * @var array
		 * @access  private
		 */
		private $options;

		/**
		 * Instances counter
		 * Usefull for setting individual "id" & "name" attributes for input[type="file"] fields and
		 * "for" attributes for their respective labels.
		 * 
		 * @var integer
		 * @access  private
		 */
		private static $instances = 0;

		/**
		 * Router calculated path for CropifyController->upload()
		 * 
		 * @var string
		 * @access  public
		 */
		public static $uploadAction;

		/**
		 * Router calculated path for CropifyController->crop()
		 * 
		 * @var string
		 * @access  public
		 */
		public static $cropAction;

		/**
		 * Router calculated path for CropifyController->delete()
		 * 
		 * @var string
		 * @access  public
		 */
		public static $deleteAction;

		/**
		 * Helpers used by Cropify->input()
		 * 
		 * @var array
		 * @access public
		 */
		public $helpers = array('Html', 'Form');


		/**
		 * Constructor
		 * 
		 * @param View  $viewFile  The view this helper is attached to.
		 * @param array $settings  Configuration settings for the helper. Possible keys:
		 *    `jQuery` to include own jQuery or not
		 *    `widget` to include own jQueryUI Widget lib or not
		 *    `uploadDir` the directory where to move the cropped images
		 *    `S3` to let the CropifyController work with the S3 plugin
		 *    `S3Bucket` the bucket where the S3 plugin will upload the images
		 * @see http://codehub.alexseman.com/s3-plugin 
		 * @access public
		 */
		public function __construct(View $viewFile, $settings = array())
		{
			parent::__construct($viewFile);
			
			$this->options = array(
										'containerClass' => '',
										'containerAfterCropClass' => '',
										'errorClass' => '',
										'buttonClass' => 'cropify-button',
										'label' => false,
										'buttonCaption' => __('Browse'),
										'secondaryCaption' => false,
										'width' => 270,
										'height' => 270,
										'S3' => false,
										'keepOriginal' => false,
										'destinationImage' => true,
										'jQuery' => false,
										'widget' => false
									);

			if($settings['jQuery'] == true)
			{
				$this->includeJQuery = true;
			}

			if($settings['widget'] == true)
			{
				$this->includeWidgetLib = true;
			}

			if(isset($settings['S3']) && $settings['S3'] == true)
			{
				$this->options['S3'] = true;

				if(isset($settings['S3Bucket']))
				{
					$this->options['S3Bucket'] = $settings['S3Bucket'];
				}
			}

			if(isset($settings['uploadDir']))
			{
				$this->uploadDir = $settings['uploadDir'];
			}
		}

		/**
		 * The paths for CropifyController's actions are calculated before the view file is rendered.
		 * 
		 * @return void
		 * @access public
		 */
		public function beforeRender($viewFile)
		{
			self::$uploadAction = Router::url(array('controller' => 'cropify/cropify', 'action' => 'upload', 'admin' => false));
			self::$cropAction = Router::url(array('controller' => 'cropify/cropify', 'action' => 'crop', 'admin' => false));
			self::$deleteAction = Router::url(array('controller' => 'cropify/cropify', 'action' => 'delete', 'admin' => false));
		}


		/**
		 * Placing the needed CSS & JS assets just before the layout is rendered.
		 * 
		 * @return void
		 * @access public
		 */
		public function afterRender($viewFile)
		{
			$this->Html->css('/cropify/lib/jcrop/css/jquery.Jcrop.min', null, array('inline' => false));
			$this->Html->css('/cropify/css/cropify', null, array('inline' => false));
			
			if($this->includeJQuery)
			{
				$this->Html->script('/cropify/lib/jquery/dist/jquery.min', array('inline' => false));
			}

			if($this->includeWidgetLib)
			{
				$this->Html->script('/cropify/lib/jqueryui/ui/minified/jquery.ui.widget.min', array('inline' => false));
			}

			$this->Html->script('/cropify/lib/jcrop/js/jquery.Jcrop', array('inline' => false));
			$this->Html->script('/cropify/js/cropify', array('inline' => false));
			$this->Html->script('/cropify/js/init-cropify', array('inline' => false));
		}


		/**
		 * The meat of the CropifyHelper
		 *
		 * It has a similar syntax to the FormHelper's inputh method: first argument is the field name
		 * and the second an array with your settings and needs (Cropify wise :).)
		 * In an editing situation where $this->data[Model][field] isn't empty it will automatically output
		 * the current image while also having the possibility to upload a new one.
		 * You can also override options set in the controller like `uploadDir`, `jQuery`, `S3` & `S3Bucket`.
		 * 
		 * @param  string $fieldName The name for the model's field. Needs to be something like: "Model.fieldName"
		 * @param  array  $opt       Options array. Possible keys:
		 *    `containerClass` the class for the input[type="file"]'s container
		 *    `containerAfterCropClass` the class for the container after cropping. It will overwrite `containerClass`
		 *    `errorClass` the class for the error notification's container
		 *    `butonClass` the class for the <<Browse>> button
		 *    `label` the name of the input's label. Set to false if no label is desired
		 *    `buttonCaption` the text content for the <<Browse>> button
		 *    `secondaryCaption` a subtext for the <<Browse>> button's caption. Set to false if not desired
		 *    `width` the desired width the image should be cropped to
		 *    `height` the desired height the image should be cropped to
		 *    `S3` useful to override to false the `S3` setting for the helper (set in the controller). Can't override to true if the `S3Bucket` setting isn't set in the controller
		 *    `keepOriginal` practical for editing: when editing the image and uploading another one, set to true to not delete the old image
		 *    `destinationImage` set to true if live output is desired when cropping
		 * @return string           The full markup needed for a Cropify upload (an instance).
		 * @access public
		 */
		public function input($fieldName, $opt = array()) {
			$inputOptions = array_merge($this->options, $opt);
			$fields = explode('.', $fieldName);
			self::$instances++;
			$output = '';
			$fieldValue = '';

			if(self::$instances < 2) {
				$output .= '<aside id="cropify-modal"' . ' data-upload-action="' . self::$uploadAction . '" data-crop-action="' . self::$cropAction . '" data-delete-action="' . self::$deleteAction . '" '  . '>';
				$output .= '<progress min="0" max="100" value="0">Uploading image&hellip;</progress>';
				$output .= '<p class="cropify-clear">';
				$output .= '<button type="button" id="cropify-cancel">Cancel</button>';
				$output .= '<button type="button" id="cropify-crop" >Crop</button>';
				$output .= '</p>';
				$output .= '</aside>';
			}
			
			$output .= '<div class="cropify-up-box ' . $inputOptions['containerClass'] . '" ' . 'data-container-after-crop-class="' . ((!empty($inputOptions['containerAfterCropClass'])) ? $inputOptions['containerAfterCropClass'] : $inputOptions['containerClass']) . '" ' . (($inputOptions['destinationImage']) ? 'data-dest-image="1"' : 'data-dest-image="0"') . ((!empty($inputOptions['errorClass'])) ? 'data-error-class="' . $inputOptions['errorClass'] . '"' : '') . '>';
			
			if($inputOptions['label'] !== false) {
				$output .= '<label for="cropify-image-' . self::$instances . '">' . $inputOptions['label'] . '</label>';
			}

			if(!empty($this->request->data[$fields[0]][$fields[1]])) {

				if(count($fields) == 2) {
					$fieldValue = $this->request->data[$fields[0]][$fields[1]];
				}
				elseif(count($fields) == 3) {
					$fieldValue = $this->request->data[$fields[0]][$fields[1]][$fields[2]];
				}

				$absolutePath = '';
				$imgAttributes = array('alt' => $fieldValue);

				if(!(strstr($fieldValue, 'http://') === false && strstr($fieldValue, 'https://') === false)) {
					$absolutePath = $fieldValue;
				}
				else {
					$absolutePath = WWW_ROOT . $this->uploadDir . basename($fieldValue);
				}

				$imgAttributes['data-absolute-path'] = $absolutePath;

				if($inputOptions['keepOriginal'])	{
					$imgAttributes['data-original'] = $absolutePath;
				}
				
				$output .= '<p>' . $this->Html->image($fieldValue, $imgAttributes) . '</p>';
			}

			$output .= '<input type="file" accept="image/*" id="cropify-image-' . self::$instances . '" name="cropify-image-' . self::$instances . '" data-dest-width="' . $inputOptions['width'] . '" data-dest-height="' . $inputOptions['height'] . '" data-upload-dir="' . $this->uploadDir . '"';
			if($inputOptions['S3'])	{
				$output .= ' data-s3-bucket="' . base64_encode($inputOptions['S3Bucket']) . '"';
			}
			$output .= ' />';

			$output .= '<button type="button" class="' . $inputOptions['buttonClass'] . '">' . $inputOptions['buttonCaption'] . (($inputOptions['secondaryCaption']) ? '<span>' . $inputOptions['secondaryCaption'] . '</span>' : '') . '</button>';
			$output .= $this->Form->hidden($fieldName);
			$output .= '</div>';


			return $output;
		}
	}
?>

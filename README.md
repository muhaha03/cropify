Cropify
=======

Functionality
-------------

Image AJAX upload & cropping plugin for CakePHP2

When work started for the plugin, it was intended for the JS functionality to be library independent so most of it is written in vanilla JavaScript. 
But a suitable library-independent replacement for _[jCrop](https://github.com/tapmodo/Jcrop/)_ couldn't be found and _jCrop_ was used so don't be put off by the mix of
simple JavaScript with jQuery bits :).

Also some of the first commits have the "Cropify:" prefix in the commit messages because the plugin was developed as part of a bigger project.


Installation
------------

1.	clone the repo to _APP/Plugin/Cropify_
2.	Create the _APP/Plugin/Cropify/webroot/uploads_ directory with read and write permissions for the "other users" group (depending on what user your webserver & PHP are)
3.	Let your application know about the plugin by appending this to your _app/Config/bootstrap.php_ file: `CakePlugin::load('Cropify');`

		
Changelog
---------

*	__v1.0__ *13 July 2012*

	* Ready for primetime: bug fixes, documented source files & added README
	
Usage
-----

1.	You need to configure the _CropifyHelper_ in your controller like this:

	* For your whole controller:
		
			public $helpers = array('Cropify.Cropify' => array('jQuery' => true, 'uploadDir' => 'img/uploads'));
	
	* Or only in the actions you need it:

			$this->helpers['Cropify.Cropify'] = array('jQuery' => true, 'uploadDir' => 'img/uploads');
												
	Where the `jQuery` boolean specifies if Cropify should include it's own jQuery library or not (in the case there will already be one in the view).
	And the `uploadDir` key specifies the `WWW_ROOT` directory where the images will be moved to after cropping.
												
	* If you want _Cropify_ to work with the _[S3 Plugin](http://codehub.alexseman.com/s3-plugin)_ you need to also add the `S3` & `S3Bucket` keys like this:
	
			public $helpers = array('Cropify.Cropify' => array(
																	'jQuery' => true, 
																	'uploadDir' => 'img/uploads', 
																	'S3' => true, 
																	'S3Bucket' => 'myS3Bucket'
																));

2.	In your view call the helper's `input()` method in a similar fashion to `$this->Form->input`:

		echo $this->Cropify->input('Model.field', array(
															'containerClass' => 'crop-container',
															'containerAfterCropClass' => 'cropped',
															'errorClass' => 'error-message',
															'buttonClass' => 'my-upload-button',
															'label' => __('Upload an image'),
															'buttonCaption' => __('Upload a few photos'),
															'secondaryCaption' => __('Try higher resolution pics'),
															'width' => 800,
															'height' => 588,
															'keepOriginal' => true,
															'destinationImage' => false
														));
														
	Parameters:
	
	*	`string` $fieldName `required`
	
		The name for the model's field. Needs to be something like: "Model.fieldName"
	
	*	`array`	 $options   `optional`
		 *	  `containerClass` the class for the `input[type="file"]`'s container
		 *    `containerAfterCropClass` the class for the container after cropping. It will overwrite `containerClass`
		 *    `errorClass` the class for the error notification's container
		 *    `butonClass` the class for the _Browse_ button
		 *    `label` the name of the input's label. Set to _false_ if no label is desired
		 *    `buttonCaption` the text content for the _Browse_ button
		 *    `secondaryCaption` a subtext for the _Browse_ button's caption. Set to _false_ if not desired
		 *    `width` the desired width the image should be cropped to
		 *    `height` the desired height the image should be cropped to
		 *    `S3` useful to override to _false_ the `S3` setting for the helper (set in the controller). Can't override to _true_ if the `S3Bucket` key isn't set in the controller
		 *    `keepOriginal` practical for editing: when editing the image and uploading another one, set to _true_ to not delete the old image
		 *    `destinationImage` set to _true_ if live output is desired when cropping
